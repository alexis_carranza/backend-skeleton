# README #

* Express.js and mongoDB backend skeleton for RESTful API.
* Version: 1.0

### Description ###
* This is a skeleton for RESTful API that contains the basic CRUD operations. At it's current stage this implementation is for a note taking app. The API uses the CRUD operations for the notes.

